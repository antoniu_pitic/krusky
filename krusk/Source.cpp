#include <iostream>
#include <fstream>
using namespace std;

struct Muchie{
	int v1,v2;
	int val;
};

int n,m;
Muchie M[100];

void Citire();
void SortareMuchii();
void SortareMuchii2();
void AfisareMuchii();
void AfisareOMuchie();
void Kruskal();
int CareMuchieEMaiMare(const void *, const void *);

int main(){
	Citire();
	AfisareMuchii();
	cout << "\n --------------------------------- \n";
	Kruskal();
}




void Citire(){
	ifstream f("graf.txt");
	f >> n >> m;
	for(int i=1;i<=m;i++){
		f >> M[i].v1 >> M[i].v2 >> M[i].val;
	}
}

void AfisareOMuchie(Muchie M){
		cout <<  "(" << M.v1 << "," << M.v2 ;
		cout << ") -> " << M.val << endl;
}

void AfisareMuchii(){
	for(int i=1;i<=m;i++){
		AfisareOMuchie(M[i]);
	}
}

void SortareMuchii(){
	for(int t=1;t<=m-1;t++){
		for(int i=1;i<m;i++){
			if (M[i].val > M[i+1].val){
				swap(M[i],M[i+1]);
			}
		}
	}
}

void SortareMuchii2(){
	qsort(M,m,sizeof(Muchie),CareMuchieEMaiMare);
}


int CareMuchieEMaiMare(const void * m1, const void * m2){
	return ((Muchie *)m1)->val - ((Muchie *)m2)->val;
}


void Kruskal(){
	int P[10];
	for(int i=1;i<=n;i++){P[i]=i;}
	SortareMuchii2();
	int k=1;
	for(int t=1;t<=n-1;t++){ // de n-1 ori
		while(P[M[k].v1] == P[M[k].v2]){k++;} // du-te pe priima uchie care nu formeaza ciclu (adica are capetele in subarbori diferiti)
		AfisareOMuchie(M[k]);
		int x1 = P[M[k].v1], x2 = P[M[k].v2]; // pune in acelasi arbore cei 2 subarbori care erau la capetele muchiei prospat introduse
		for(int i=1;i<=n;i++){
			if (P[i] == x2){
				P[i]=x1;
			}
		}
		k++; // treci la urmatoarea muchie
	}
}